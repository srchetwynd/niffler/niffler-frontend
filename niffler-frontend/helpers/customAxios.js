import axios from "axios";
import router from "../src/router";
import store from "../src/store/store";
import config from "../config";

function axiosFactory(mockAxios) {
    if (!mockAxios) {
        let customAxios = axios.create({
            baseURL: config.baseURL
        });
        customAxios.interceptors.request.use((config) => {
            config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
            return config;
        });

        customAxios.interceptors.response.use((response) => {
            return response;
        }, (err) => {
            if (err.response.status === 403) {
                store.commit("setLoggedIn", false);
                store.dispatch("resetAll");
                router.push({ path: "/" });
            } else {
                return err;
            }
        });

        return customAxios;
    }
}

export default axiosFactory;
