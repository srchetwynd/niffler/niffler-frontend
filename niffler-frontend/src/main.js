import Vue from "vue";
import * as Sentry from "@sentry/browser";
import * as Integrations from "@sentry/integrations";

import App from "./App.vue";
import router from "./router";
import store from "./store/store";

import "./registerServiceWorker";
import vuetify from "./plugins/vuetify";

Sentry.init({
    dsn: "https://3e84eb9095cb43ae9d714d5f35a745d3@sentry.io/1505168",
    integrations: [
        new Integrations.Vue({
            Vue,
            attachProps: true,
            logErrors: true
        })
    ]
});

Vue.config.productionTip = false;

Vue.prototype.$sentry = Sentry;

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount("#app");
