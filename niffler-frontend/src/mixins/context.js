import moment from "moment";

export default {
    data: function() {
        return {
            localContext: {
                trench: null,
                feature: null,
                context_type: null,
                recorded_by: null,
                checked_by: null,
                recorded_date: moment().format("YYYY-MM-DD"),
                checked_date: null,
                number: null,
                project: null,
                site: null
            },
            aboves: [],
            belows: [],
            same_as: [],
            photo_context: [],
            drawing_context: [],
            addedContext: null
        };
    },
    computed: {
        /**
         * Current project
         *
         * @returns {Object} Current Project
         */
        project() {
            return this.$store.getters.getCurrentProject;
        },
        /**
         * Current site
         *
         * @returns {Object} Current Site
         */
        site() {
            return this.$store.getters.getCurrentSite;
        },
        /**
         * Current trench
         *
         * @returns {Object} Current Trench1
         */
        trench() {
            return this.$store.getters.getCurrentTrench;
        },
        /**
         * Contexts
         *
         * gets the contexts for the current trench
         *
         * @returns {Object} of current trench contexts
         */
        contexts: function() {
            return this.$store.state.contexts;
        },
        /**
         * User
         *
         * gets user from the store
         *
         * @returns {Object} of current user
         */
        user: function() {
            return this.$store.state.user;
        },
        /**
         * Possible aboves,
         *
         * Fileters through all the contexts for this trench and returns all
         * contexts not currently set as below this one, or this one
         *
         * @return {Array} of context objects
         */
        possibleAbove: function() {
            if (typeof this.contexts === "undefined") {
                return [];
            } else {
                return this.contexts.data
                    .filter(context => {
                        return (
                            !this.belows.includes(context.id) &&
                            !this.aboves.includes(context.id) &&
                            context.attributes.id !== this.localContext.number
                        );
                    })
                    .map(element => {
                        return {
                            text: element.attributes.number.toString(),
                            value: element.id
                        };
                    });
            }
        },
        /**
         * Possible Belows,
         *
         * Filters through all the contexts in this trench and returns all contexts
         * not currently set as above this one or this one
         */
        possibleBelows: function() {
            if (typeof this.contexts === "undefined") {
                return [];
            } else {
                return this.contexts.data
                    .filter(context => {
                        return (
                            !this.aboves.includes(context.id) &&
                            !this.belows.includes(context.id) &&
                            context.attributes.number !==
                                this.localContext.number
                        );
                    })
                    .map(element => {
                        return {
                            text: element.attributes.number.toString(),
                            value: element.id
                        };
                    });
            }
        },
        /**
         * Current Context
         *
         * if the current context is null return message string
         *
         * @returns {String}
         */
        currentContextNumber: function() {
            if (this.localContext.number === null) {
                return "Set on Save";
            } else {
                return this.localContext.number;
            }
        }
    },
    methods: {
        /**
         * Set the localContexted Checked date
         */
        setChecked() {
            this.localContext.checked_date = moment().format("YYYY-MM-DD");
        },
        /**
         * Event listener for the aboves dropdown
         *
         * @param {Object}
         */
        addAbove(above) {
            if (typeof above.value !== "undefined") {
                this.aboves.push(above.value);
            }
        },
        /**
         * Event listener for the belows dropdown
         *
         * @param {Object}
         */
        addBelow(below) {
            if (typeof below.value !== "undefined") {
                this.belows.push(below.value);
            }
        },
        /**
         * Gets the context object from the id
         *
         * @param {Integer} ID the id of the context
         * @returns {Object<Context>}
         */
        getContextFromID(ID) {
            return this.contexts.data.find(element => {
                return element.id === ID;
            });
        }
    }
};
