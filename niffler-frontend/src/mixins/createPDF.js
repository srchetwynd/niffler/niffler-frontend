import pdfjs from "pdfjs";

export default {
    methods: {
        createPDFHeader(table, context) {
            let tr = table.row();
            tr.cell({ colspan: 2 })
                .text("Project:\n", { fontSize: 8 })
                .text(context.project, { textAlign: "center" });
            tr.cell()
                .text("Site Number:\n", { fontSize: 8 })
                .text(`${context.site}`, { textAlign: "center" });
            tr.cell()
                .text("Trench Number:\n", { fontSize: 8 })
                .text(`${context.trench}`, { textAlign: "center" });
            tr.cell()
                .text("Context Type:\n", { fontSize: 8 })
                .text(context.context_type, { textAlign: "center" });
            tr.cell()
                .text("Context Number:\n", { fontSize: 8 })
                .text(`${context.number}`, { textAlign: "center" });
        },

        createPDFMatrix(table, number, aboves, belows) {
            let tr = table.row();
            tr.cell({ colspan: 6, minHeight: 69 })
                .text("Matrix:\n", { fontSize: 8 })
                .text(aboves.toString(), { textAlign: "center" })
                .text("This Context Is: ", { fontSize: 8 })
                .add(`${number}`, { fontSize: 12 })
                .text(belows.toString(), { textAlign: "center" });
        },

        createPDFRecordedChecked(table, recordedChecked) {
            let tr = table.row();
            let recordedBy = this.allUsers.find(element => {
                return element.id === recordedChecked.recorded_by;
            });
            recordedBy = recordedBy
                ? recordedBy.attributes.email_address
                : { attributes: { email_address: "" } };

            let checkedBy = this.allUsers.find(element => {
                return element.id === recordedChecked.checked_by;
            });
            checkedBy = checkedBy
                ? checkedBy.attributes.email_address
                : { attributes: { email_address: "" } };

            tr.cell({ colspan: 3 })
                .text("Recorded By: ", { fontSize: 8 })
                .add(recordedBy, { fontSize: 12 })
                .text("Date Recorded: ", { fontSize: 8 })
                .add(recordedChecked.recorded_date, { fontSize: 12 });

            tr.cell({ colspan: 3 })
                .text("Checked By: ", { fontSize: 8 })
                .add(checkedBy, { fontSize: 12 })
                .text("Checked Date: ", { fontSize: 8 })
                .add(recordedChecked.checked_date, { fontSize: 12 });
        },

        createPDFSameAsPhotos(table, sameAs, photos) {
            let tr = table.row();
            tr.cell({ colspan: 6 })
                .text("Context same as: ", { fontSize: 8 })
                .add(sameAs.toString(), { fontSize: 12 });

            tr = table.row();
            tr.cell({ colspan: 6 })
                .text("Photos: ", { fontSize: 8 })
                .add(photos.toString(), { fontSize: 12 });
        },

        createDepositCut(table, sectionTitles, context, contextType) {
            const contextDetails = context.attributes.details;

            let text = sectionTitles.title + "\n";
            let contextTextDetails = "";
            let i = 1;
            // eslint-disable-next-line no-unused-vars
            for (let key in sectionTitles) {
                if (key !== "title") {
                    text += i + ". " + sectionTitles[key] + "\n";
                    if (!contextDetails[key]) {
                        contextTextDetails += i + ". Not recorded" + "\n";
                    } else {
                        contextTextDetails +=
                            i + ". " + contextDetails[key] + "\n";
                    }
                    i++;
                }
            }

            this.createPDFHeader(table, context.attributes.context);

            let tr = table.row();
            tr.cell({ minHeight: 327 }).text(text, { fontSize: 8 });
            tr.cell({ colspan: 5 }).text(contextTextDetails, { fontSize: 8 });

            this.createPDFMatrix(
                table,
                context.attributes.context.number,
                context.attributes.aboves,
                context.attributes.belows
            );

            tr = table.row();
            tr.cell({ colspan: 6 })
                .text("Your Interpretation:", { fontSize: 8 })
                .add(contextDetails.interpretation, { fontSize: 12 })
                .text(contextDetails.interpretation_comment);

            tr = table.row();
            tr.cell({ colspan: 6, minHeight: 120 })
                .text("Your discussion:", { fontSize: 8 })
                .text(contextDetails.discussion);

            this.createPDFSameAsPhotos(
                table,
                context.attributes.same_as,
                context.attributes.photos
            );

            tr = table.row();
            tr.cell({ colspan: 6 })
                .text("Samples: ", { fontSize: 8 })
                .add("Samples", { fontSize: 12 });

            tr = table.row();
            tr.cell({ colspan: 6 })
                .text("Finds: ", { fontSize: 8 })
                .add("     ")
                .add("None", { fontSize: 12, strikethrough: true })
                .add("     ")
                .add("Pot", { fontSize: 12, strikethrough: false })
                .add("     ")
                .add("Bone", { fontSize: 12, strikethrough: true })
                .add("     ")
                .add("Glass", { fontSize: 12, strikethrough: true })
                .add("     ")
                .add("Metal", { fontSize: 12, strikethrough: true })
                .add("     ")
                .add("CBM", { fontSize: 12, strikethrough: true })
                .add("     ")
                .add("Other BM", { fontSize: 12, strikethrough: true })
                .add("     ")
                .add("Wood", { fontSize: 12, strikethrough: true })
                .add("     ")
                .add("Leather", { fontSize: 12, strikethrough: true });

            tr = table.row();
            tr.cell({ colspan: 3 })
                .text("Finds Sieving: ", { fontSize: 8 })
                .add("        ")
                .add("On Site", { fontSize: 12, strikethrough: false })
                .add("        ")
                .add("Off Site", { fontSize: 12, strikethrough: false })
                .add("        ")
                .add("In Situ", { fontSize: 12, strikethrough: false });
            tr.cell({ colspan: 3 })
                .text("Metal Detecting: ", { fontSize: 8 })
                .add("        ")
                .add("On Site", { fontSize: 12, strikethrough: false })
                .add("        ")
                .add("Off Site", { fontSize: 12, strikethrough: false })
                .add("        ")
                .add("In Situ", { fontSize: 12, strikethrough: false });

            this.createPDFRecordedChecked(table, context.attributes.context);
        },

        createPDF(contextType, context) {
            console.log(contextType, context);
            let doc = new pdfjs.Document();
            doc.footer().text("DigABase", { textAlign: "center" });
            var table = doc.table({
                widths: [91, 91, 91, 91, 91, 91],
                borderWidth: 1,
                padding: 5
            });
            var depositText = {
                title: "DEPOSIT",
                compaction: "Compaction",
                colour: "Colour",
                composition: "Composition",
                inclusions: "Inclusions",
                thickness: "Thickness",
                weather: "Weather",
                method: "Method",
                comments: "Comments"
            };
            var cutText = {
                title: "CUT",
                shape_in_plan: "Shape in Plan",
                corners: "Corners",
                dimensions_depth: "Dimensions / Depth",
                break_of_slope_top: "Break of slope - Top",
                sides: "Sides",
                break_of_slope_base: "Break of Slope - Base",
                orientation: "Orientation",
                inclination_of_axis: "Inclination of Axis",
                truncated: "Truncated",
                comments: "Comments"
            };

            var masonryText = {
                title: "MASONRY",
                materials: "Materials",
                size_of_materials: "Size of Materials",
                finish: "Finish",
                bond: "Bond",
                direction: "Direction",
                bonding_material: "Bonding Material",
                dimensions: "Dimensions",
                worked_stones: "Worked Stones",
                other_comments: "Other Comments"
            };

            var timberText = {
                title: "TIMBER",
                type: "Type",
                setting: "Setting",
                orientation: "Orientation",
                cross_section: "Cross Section",
                condition: "Condition",
                dimensions: "Dimensions",
                convertsion: "Convertsion",
                tool_markings: "Tool Markings"
            };

            if (contextType === "Deposits") {
                this.createDepositCut(table, depositText, context, contextType);
            } else if (contextType === "Cuts") {
                this.createDepositCut(table, cutText, context, contextType);
            } else if (contextType === "Masonry") {
                this.createDepositCut(table, masonryText, context, contextType);
            } else if (contextType === "Timbers") {
                this.createDepositCut(table, timberText, context, contextType);
            }

            doc.asBuffer().then(buf => {
                const blob = new Blob([buf], { type: "application/pdf" });
                this.pdf = URL.createObjectURL(blob);
                this.showPDF = true;
            });
        }
    }
};
