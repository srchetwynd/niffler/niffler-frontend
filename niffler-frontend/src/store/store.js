import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import resource from "./modules/resource";
import cachedResource from "./modules/cachedResource";

import axiosFactory from "../../helpers/customAxios";
const axiosInstance = axiosFactory();

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        project: resource("Project"),
        site: resource("Site"),
        trench: resource("Trench"),
        search: resource("Search"),
        coffin: resource("Coffin"),
        cut: resource("Cut"),
        deposit: resource("Deposit"),
        masonry: resource("Masonry"),
        skeleton: resource("Skeleton"),
        timber: resource("Timber"),
        photo: resource("Photo"),
        users: resource("User"),
        point: resource("Point"),
        polyline: resource("Polyline"),
        polygon: resource("Polygon"),
        photoType: cachedResource("PhotoType"),
        orientation: cachedResource("Orientation"),
        degreeOfContamination: cachedResource("DegreeOfContamination"),
        interpretation: cachedResource("Interpretation"),
        metalDetectorLocation: cachedResource("MetalDetectorLocation"),
        sievingLocation: cachedResource("SievingLocation"),
        unit: cachedResource("Unit")
    },
    state: {
        notification: null,
        allUsers: null,
        matrix: null,
        message: "",
        messageType: "",
        photoTypes: null,
        orientations: null,
        showMessage: false,
        user: null,
        error: null,
        loading: 0,
        loaded: 0,
        loggedIn: false
    },
    getters: {
        getNotification(state) {
            return state.notification;
        },
        loading(state) {
            return {
                total: state.loading,
                loaded: state.loaded
            };
        },
        getLoggedIn(state) {
            return state.loggedIn;
        }
    },
    mutations: {
        setLoggedIn(state, val) {
            state.loggedIn = val;
        },
        setNotification(state, val) {
            state.notification = val;
        },
        setMatrix(state, val) {
            state.matrix = val;
        },
        setUser(state, val) {
            state.user = val;
        },
        setError(state, val) {
            state.error = val;
        },
        setLoading(state, val) {
            if (val) {
                state.loading++;
            } else {
                state.loaded++;
            }
        },
        resetLoading(state) {
            state.loading = 0;
            state.loaded = 0;
        }
    },
    actions: {
        getContextsFromServer({ commit, state }, { url, type }) {
            return axiosInstance.get(url).then(response => {
                commit("setContexts", { type, data: response.data });
                return "done";
            });
        },
        async getCurrentURL(
            { commit, dispatch, state, getters },
            { projectID, siteID, trenchID, contextID, refresh = false } = {}
        ) {
            if (refresh) {
                commit("project/setAllProject", null);
                commit("site/setAllSite", null);
                commit("trench/setAllTrench", null);
                commit("project/setAProject", null);
                commit("site/setASite", null);
                commit("trench/setATrench", null);
            }

            dispatch("users/fetchAllUser", {
                link: state.user.data.relationships.user
            });

            if (!state.project.allProject) {
                await dispatch("project/fetchAllProject", {
                    link: state.user.data.relationships.project
                });
            }

            if (
                projectID &&
                (!state.project.aProject ||
                    state.project.aProject.data.id !== +projectID)
            ) {
                let project = await dispatch("project/fetchAProject", {
                    link: state.user.data.relationships.project,
                    options: {
                        params: {
                            id: projectID
                        }
                    }
                });
                console.log(project);
                dispatch("site/fetchAllSite", {
                    link: project.data.relationships.site
                });
            }

            if (
                siteID &&
                (!state.site.aSite || state.site.aSite.data.id !== +siteID)
            ) {
                let site = await dispatch("site/fetchASite", {
                    link: state.project.aProject.data.relationships.site,
                    options: {
                        params: {
                            id: siteID
                        }
                    }
                });
                dispatch("trench/fetchAllTrench", {
                    link: site.data.relationships.trench
                });
            }

            if (
                trenchID &&
                (!state.trench.aTrench ||
                    state.trench.aTrench.data.id !== +trenchID)
            ) {
                let trench = await dispatch("trench/fetchATrench", {
                    link: state.site.aSite.data.relationships.trench,
                    options: {
                        params: {
                            id: trenchID
                        }
                    }
                });
                console.log(trench);
                dispatch("coffin/fetchAllCoffin", {
                    link: trench.data.relationships.coffin
                });
                dispatch("cut/fetchAllCut", {
                    link: trench.data.relationships.cut
                });
                dispatch("deposit/fetchAllDeposit", {
                    link: trench.data.relationships.deposit
                });
                dispatch("masonry/fetchAllMasonry", {
                    link: trench.data.relationships.masonry
                });
                dispatch("skeleton/fetchAllSkeleton", {
                    link: trench.data.relationships.skeleton
                });
                dispatch("timber/fetchAllTimber", {
                    link: trench.data.relationships.timber
                });
                dispatch("photo/fetchAllPhoto", {
                    link: trench.data.relationships.photo
                });
            }
            return "done";
        },
        getMatrix({ commit, state }, trenchId) {
            axiosInstance
                .get(`trenchs/${trenchId}/context_above_below`)
                .then(response => {
                    if (response.status === 200) {
                        commit("setMatrix", response.data);
                    }
                });
        }
    },
    plugins: [createPersistedState()]
});
