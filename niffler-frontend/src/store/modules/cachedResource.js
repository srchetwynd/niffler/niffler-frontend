import axiosFactory from "../../../helpers/customAxios";
const axios = axiosFactory();

export default function(resource) {
    return {
        namespaced: true,
        state: {
            [resource]: null
        },
        getters: {
            ["get" + resource](state) {
                return state[resource] ? state[resource] : {};
            }
        },
        mutations: {
            ["set" + resource](state, val) {
                state[resource] = val;
            },
            reset(state) {
                state[resource] = null;
            }
        },
        actions: {
            resetAll: {
                root: true,
                handler({ commit }) {
                    commit("reset");
                }
            },
            ["fetch" + resource]({ commit, state }, { link, options = {} }) {
                if (state[resource]) {
                    return new Promise(resolve => resolve(state[resource]));
                }
                commit("setLoading", true, { root: true });
                return axios
                    .get(link.href, options)
                    .then(response => {
                        commit("set" + resource, response.data);
                        return response.data;
                    })
                    .catch(err => {
                        commit("setError", err, { root: true });
                        return err;
                    })
                    .finally(() => {
                        commit("setLoading", false, { root: true });
                    });
            }
        }
    };
}
