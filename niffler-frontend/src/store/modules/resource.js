import axiosFactory from "../../../helpers/customAxios";
const axios = axiosFactory();

export default function(resource) {
    return {
        namespaced: true,
        state: {
            ["a" + resource]: null,
            ["all" + resource]: null,
            ["aPromise" + resource]: null,
            ["allPromise" + resource]: null
        },
        getters: {
            ["getA" + resource](state) {
                return state["a" + resource] ? state["a" + resource] : {};
            },
            ["getAll" + resource](state) {
                return state["all" + resource] ? state["all" + resource] : {};
            }
        },
        mutations: {
            ["setA" + resource](state, val) {
                state["a" + resource] = val;
            },
            ["setAll" + resource](state, val) {
                state["all" + resource] = val;
            },
            reset(state) {
                state["a" + resource] = null;
                state["all" + resource] = null;
                state["aPromise" + resource] = null;
                state["allPromise" + resource] = null;
            }
        },
        actions: {
            resetAll: {
                root: true,
                handler({ commit }) {
                    commit("reset");
                }
            },
            ["fetchA" + resource](
                { commit, state },
                { link, options = {} } = {}
            ) {
                if (state["aPromise" + resource]) {
                    return state["aPromise" + resource];
                }
                commit("setLoading", true, { root: true });
                state["aPromise" + resource] = axios
                    .get(link.href, options)
                    .then(response => {
                        commit("setA" + resource, response.data);
                        return response.data;
                    })
                    .catch(err => {
                        commit("setError", err, { root: true });
                        return err;
                    })
                    .finally(() => {
                        commit("setLoading", false, { root: true });
                        state["aPromise" + resource] = null;
                    });
                return state["aPromise" + resource];
            },
            ["fetchAll" + resource](
                { commit, state },
                { link, options = {} } = {}
            ) {
                if (state["allPromise" + resource]) {
                    return state["allPromise" + resource];
                }
                commit("setLoading", true, { root: true });
                state["allPromise" + resource] = axios
                    .get(link.href, options)
                    .then(response => {
                        commit("setAll" + resource, response.data);
                        return response.data;
                    })
                    .catch(err => {
                        commit("setError", err, { root: true });
                        return err;
                    })
                    .finally(() => {
                        commit("setLoading", false, { root: true });
                        state["allPromise" + resource] = null;
                    });
                return state["allPromise" + resource];
            },
            ["createA" + resource]({ commit }, { payload, link }) {
                commit("setLoading", true, { root: true });
                return axios
                    .post(link.href, payload)
                    .then(response => {
                        commit("setLoading", false, { root: true });
                        return response.data;
                    })
                    .catch(err => {
                        commit("setError", err, { root: true });
                        return err;
                    })
                    .finally(() => {
                        commit("setLoading", false, { root: true });
                    });
            },
            ["putA" + resource]({ commit }, { payload, link }) {
                commit("setLoading", true, { root: true });
                return axios
                    .put(link.href, payload)
                    .then(response => {
                        commit("setLoading", false, { root: true });
                        return response.data;
                    })
                    .catch(err => {
                        commit("setError", err, { root: true });
                        return err;
                    })
                    .finally(() => {
                        commit("setLoading", false, { root: true });
                    });
            },
            ["deleteA" + resource]({ commit }, link) {
                commit("setLoading", true, { root: true });
                return axios
                    .delete(link.href)
                    .then(response => {
                        commit("setLoading", false, { root: true });
                        return response.data;
                    })
                    .catch(err => {
                        commit("setError", err, { root: true });
                        return err;
                    })
                    .finally(() => {
                        commit("setLoading", false, { root: true });
                    });
            }
        }
    };
}
