import Vue from "vue";
import Router from "vue-router";
import store from "./store/store";

Vue.use(Router);

let router = new Router({
    routes: [
        {
            path: "/",
            name: "login",
            component: () => import("./views/Login")
        },
        {
            path: "/project/:projectid/site/:siteid/trenchs/:trenchid/gis",
            name: "gis",
            props: true,
            component: () => import("./views/Gis")
        },
        {
            path: "/project/:projectid/site/:siteid/trenchs/:trenchid/matrix",
            name: "matrix",
            props: true,
            component: () => import("./views/Matrix")
        },
        {
            path:
                "/project/:projectid/site/:siteid/trenchs/:trenchid/contexts/:contextType/:contextNumber",
            name: "context",
            props: true,
            component: () => import("./views/Context")
        },
        {
            path: "/project/:projectid/site/:siteid/trenchs/:trenchid/contexts",
            name: "trench-options",
            props: true,
            component: () => import("./views/TrenchOptions")
        },
        {
            path: "/project/:projectid/site/:siteid/trenchs",
            name: "trenchs",
            props: true,
            component: () => import("./views/Trenchs")
        },
        {
            path: "/project/:projectid/site",
            name: "sites",
            props: true,
            component: () => import("./views/Sites")
        },
        {
            path: "/projects",
            name: "projects",
            component: () => import("./views/Projects")
        },
        {
            path: "/users",
            name: "users",
            component: () => import("./views/Users")
        }
    ]
});

router.beforeEach((to, from, next) => {
    store.commit("resetLoading");
    next();
});

export default router;
