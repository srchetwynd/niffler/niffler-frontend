module.exports = {
    root: true,
    env: {
        node: true
    },
    extends: [
        "plugin:vue/essential",
        "plugin:prettier/recommended",
        "@vue/standard"
    ],
    rules: {
        "prettier/prettier": "error",
        "vue/html-indent": ["error", 4],
        semi: ["error", "always"],
        indent: ["error", 4],
        quotes: ["error", "double"],
        "space-before-function-paren": ["error", "never"],
        "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
        "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off"
    },
    parserOptions: {
        parser: "babel-eslint"
    }
};
